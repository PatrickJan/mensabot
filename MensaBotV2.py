﻿import time
import configparser
from datetime import time as dtime, timedelta, datetime

import telegram
from telegram.ext import Updater, CommandHandler
from telegram.error import TelegramError, Unauthorized, BadRequest, TimedOut, NetworkError

from MensaBotV2_extra import logger, subStart, subStop, subPath, writeFileMenu, getTime, key, daily
from MensaBotV2_enet import getMensaPlan, prettifyTelegram


# Alle hier vorhandenen Funktionen sind für den Bot<->Client Interaktionen gedacht


def saveMenuPlan(bot=0, job=0):
    if daily:
        if getMensaPlan():
            writeFileMenu()
    else:
        logger("savedailymenu deaktiviert", 1)


def saveMenuVars(bot=0, job=0):
    global MenuTel

    if(job != 0 and getTime() <= "14"):
        job.schedule_removal()
        logger("HighTrafficMode beendet", 2)

    if(getTime("day") == "Samstag"):
        MenuTel = prettifyTelegram(2)
    elif(getTime("day") == "Sonntag" or getTime() >= "15"):
        MenuTel = prettifyTelegram(1)
    else:
        MenuTel = prettifyTelegram()

    logger("Das Menü wurde geladen", 2)


def mensamenu(bot, update):  # Zeigt das heutige Menü
    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    bot.sendMessage(chat_id=update.message.chat_id, text=MenuTel, parse_mode=telegram.ParseMode.HTML, disable_web_page_preview=True)

    logger("Menu wurde von ID " + str(update.message.from_user.id) + " genutzt", 2)


def helpmenu(bot, update):  # Zeigt die Chatbefehle
    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    bot.sendMessage(chat_id=update.message.chat_id, text="/help : \n Zeigt dir diesen Text an und listet darüber hinaus alle Befehle und deren Funktionen  auf. \n\n/menu : \n Zeigt dir das Menü des heutigen Tages an. Nach 15 Uhr wird dir das morgige Menü angezeigt, am Wochenende wird das Menü am Montag gezeigt\n\n/startSub :\nStartet das MenüAbo welches dir in der Woche, täglich um ca 11 Uhr das aktuelle Menü zusendet.\n\n/stopSub :\nStopt bzw Entfernt dein MenüAbo und bekommst vom Bot keine täglichen nachrichten mehr. \n", parse_mode=telegram.ParseMode.MARKDOWN)

    logger("Help wurde von ID " + str(update.message.from_user.id) + " genutzt", 2)


def subscriptionStart(bot, update):  # Startet die Subscription
    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    if(subStart(update)):
        bot.sendMessage(chat_id=update.message.chat_id, text="Du hast dich Erfolgreich zum Abo eingetragen\nDir wird nun täglich um 11 Uhr das aktuelle Menü geschickt", parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Du bist bereits im Abo eingetragen\nWenn du dich Löschen lassen willst schau unter /help nach dem richtigen Befehl", parse_mode=telegram.ParseMode.MARKDOWN)

    logger("subscriptionStart wurde von ID " + str(update.message.from_user.id) + " genutzt", 2)


def subscriptionStop(bot, update):  # Stoppt die Subscription
    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    if(subStop(update)):
        bot.sendMessage(chat_id=update.message.chat_id, text="Du hast dich Erfolgreich aus dem Abo ausgetragen\nDir wird nun nicht mehr das Menu um 11 Uhr geschickt", parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Du bist nicht im Abo eingetragen\nWenn du dich Eintragen lassen willst schau unter /help nach dem richtigen Befehl", parse_mode=telegram.ParseMode.MARKDOWN)

    logger("subscriptionStop wurde von ID " + str(update.message.from_user.id) + " genutzt", 2)


def subscriptionNotify(bot, job):  # Sendet das Menü an alle Subscriber
    if not getTime("day") == "Samstag" or getTime("day") == "Sonntag":
        if getMensaPlan():

            sub = configparser.ConfigParser()
            sub.read_file(open(subPath))
            logger("Der Tägliche Mensaplan wird versendet", 3)

            for chatID in sub.sections()[1:]:
                if sub[chatID].getboolean("act"):

                            logger("Der Tägliche Mensaplan wird an " + chatID + " versendet", 2)
                            bot.sendMessage(chat_id=chatID, text=MenuTel, parse_mode=telegram.ParseMode.HTML, disable_web_page_preview=True)
                            time.sleep(5)

    logger("Der Tägliche Mensaplan wurde versendet", 3)


def TrafficMenu(bot, job):
    job.job_queue.run_repeating(callback=saveMenuVars, interval=timedelta(minutes=5), first=datetime.now())
    logger("HighTrafficMode gestartet", 2)


def error_callback(bot, update, error):

    try:
        raise error
    except Unauthorized:
        # remove update.message.chat_id from conversation list
        logger("Unauthorized-!-!", 5)
    except BadRequest:
        # handle malformed requests - read more below!
        logger("BadRequest-!-!", 5)
    except TimedOut:
        # handle slow connection problems
        logger("TimedOut-!-!", 5)
    except NetworkError:

        logger("NetworkError - Es läuft eine andere BOT instanz", 5)

    except TelegramError:
        # handle all other telegram related errors
        logger("TelegramError-!-!", 5)


def main():
    updater = Updater(key)
    dp = updater.dispatcher

    # Handler
    dp.add_handler(CommandHandler("m", mensamenu))
    dp.add_handler(CommandHandler("menu", mensamenu))
    dp.add_handler(CommandHandler("?", helpmenu))
    dp.add_handler(CommandHandler("h", helpmenu))
    dp.add_handler(CommandHandler("help", helpmenu))
    dp.add_handler(CommandHandler("start", subscriptionStart))
    dp.add_handler(CommandHandler("startSub", subscriptionStart))
    dp.add_handler(CommandHandler("startsub", subscriptionStart))
    dp.add_handler(CommandHandler("stop", subscriptionStop))
    dp.add_handler(CommandHandler("stopSub", subscriptionStop))
    dp.add_handler(CommandHandler("stopsub", subscriptionStop))

    dp.add_error_handler(error_callback)

    # Queue
    updater.job_queue.run_daily(saveMenuPlan, dtime(10, 00))                # Datei Speichern
    updater.job_queue.run_daily(saveMenuVars, dtime(10, 50))                # MenüVars
    updater.job_queue.run_daily(subscriptionNotify, dtime(11, 00))          # MenüAbo senden0
    updater.job_queue.run_daily(TrafficMenu, dtime(11, 00))                 # MenüVars hightraffic
    updater.job_queue.run_daily(saveMenuVars, dtime(15, 00))                # MenüVars

    # Bot rest
    logger("Der Bot wurde gestartet", 2)

    saveMenuVars()

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
