import arrow
import configparser
import json
import coloredlogs
import logging
from logging.handlers import TimedRotatingFileHandler
import os
import sys


# Alle hier vorhanden Funktionen und Variablen sind zum Speichern und Verwalten gedacht

config = configparser.ConfigParser()
tokenName = "token.cfg"

try:
    config.read_file(open(tokenName))
except FileNotFoundError:
    print(tokenName + " wurde nicht gefunden")

    config["API"] = {"telegramBotAPI": "None"}

    config["LOGGING"] = {"activatelogging": "true",
                         "savelogging": "true",
                         "loggingDirectory": "log",
                         "loggingLevel": "1     # 1 'DEBUG' 2 'INFO', 3 'WARNING', 4 'ERROR', 5 'CRITICAL'",
                         "coloredLogging": "false"}

    config["MENU"] = {"saveDailyMenu": "true",
                      "saveMenuDirectory": "mensaplan",
                      "subsciberFileName": "subscriber.cfg"}

    with open(tokenName, "w") as defaultconfig:
        config.write(defaultconfig)

        print("Erstelle neue " + tokenName + " - Programm Gestoppt")
        exit()

try:
    config["LOGGING"].getboolean("activatelogging")
    config["LOGGING"].getboolean("savelogging")
    config["LOGGING"].getboolean("coloredLogging")
    config["MENU"].getboolean("savedailymenu")

except ValueError:
    print(tokenName + " einstellungen sind ungültig - (true/false) Fehler")
    exit()

key = config["API"]["telegramBotAPI"]
daily = config["MENU"].getboolean("savedailymenu")

subFile = os.path.join(sys.path[0], config["MENU"]["subsciberfilename"])
if not os.path.isfile(subFile):
    sub = configparser.ConfigParser()
    sub["INFO"] = {"usrcount": 0,
                   "subcount": 0}

    with open(subFile, "w") as defaultsub:
        sub.write(defaultsub)

        print("Erstelle neue" + config["MENU"]["subsciberfilename"])

mensaPath = os.path.join(sys.path[0], config["MENU"]["savemenudirectory"])
subPath = os.path.join(sys.path[0], config["MENU"]["subsciberfilename"])

if config["LOGGING"].getboolean("activatelogging"):

    log = logging.getLogger("MensaBotV2")
    log.setLevel(int(config["LOGGING"]["logginglevel"][0]) * 10)

    if config["LOGGING"].getboolean("coloredlogging"):
        coloredlogs.install(logger=log, level_styles={'critical': {'color': 'red', 'bold': True}, 'debug': {'color': 'green'}, 'error': {'color': 'red'}, 'info': {}, 'notice': {'color': 'magenta'}, 'spam': {'color': 'green', 'faint': True}, 'success': {'color': 'green', 'bold': True}, 'verbose': {'color': 'blue'}, 'warning': {'color': 'yellow'}}, field_styles={'asctime': {'color': 'green'}, 'hostname': {'color': 'magenta'}, 'levelname': {'color': 'black', 'bold': True}, 'name': {'color': 'blue'}, 'programname': {'color': 'cyan'}})

    if config["LOGGING"].getboolean("savelogging"):

        logdir = os.path.join(sys.path[0], config["LOGGING"]["loggingDirectory"])

        if not os.path.isdir(logdir):
            os.makedirs(logdir)

        logname = os.path.join(logdir, arrow.now("Europe/Berlin").strftime("%Y-%m-%d_%H-%M-%S" + "-MensaBot.log"))
        handler = TimedRotatingFileHandler(logname, when="midnight")
        log.addHandler(handler)

        logging.basicConfig(format="[%(name)s] - %(asctime)s - %(levelname)s - %(message)s", filename=logname, filemode="w")
    else:
        logging.basicConfig(format="[%(name)s] - %(asctime)s - %(levelname)s - %(message)s", stream=sys.stdout)


def logger(argument, level=1):

    if(level == 1):
        log.debug(argument)
    elif(level == 2):
        log.info(argument)
    elif(level == 3):
        log.warning(argument)
    elif(level == 4):
        log.error(argument)
    elif(level == 5):
        log.critical(argument)


def getUserData(argument):

    userData = ""

    if(argument.first_name):
        userData += "Vorname: " + argument.first_name + " "

    if(argument.last_name):
        userData += "Nachname: " + argument.last_name + " "

    if(argument.username):
        userData += "Username: " + argument.username

    return userData


def getTime(timef="", argument=0):

    Wochentage = [" ", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"]
    time = arrow.now("Europe/Berlin")

    if argument:
        time = time.shift(days=+argument)

    if timef is "":
        time = time.format("H")
        logger("Time wurde abgefragt", 1)

    elif timef is "day":
        time = Wochentage[int(time.format("d"))]
        logger("Tag wurde abgefragt", 1)

    elif timef is "doy":
        time = time.shift(days=-1).format("YYYYDDDD")  # -1 weil Webseite um einen Tag verschoben ist
        logger("DoY wurde abgefragt", 1)

    elif timef is "date":
        time = time.format("YYYY-MM-DD")  # dieses Datumsformat wird so auf der Webseite verwendet
        logger("Date wurde abgefragt", 1)

    elif timef is "dateEU":
        time = time.format("DD.MM.YYYY")
        logger("DateEU wurde abgefragt", 1)

    return time


def writeFileMenu(argument=0):
    from MensaBotV2_enet import getMensaPlan
    menu = getMensaPlan(argument)

    date = getTime("date", argument).split("-")
    year = os.path.join(mensaPath, date[0])
    month = os.path.join(year, date[1])
    day = os.path.join(month, date[2])

    if not os.path.isdir(year):
        os.makedirs(year)
        logger("Das Jahr " + str(date[0]) + " existerte noch nicht", 2)

    if not os.path.isdir(month):
        os.makedirs(month)
        logger("Der Monat " + str(date[1]) + " existerte noch nicht", 2)

    with open(day + ".json", "a+", encoding="utf-8") as file:
        json.dump(menu, file, ensure_ascii=False, sort_keys=True, indent=4, separators=(",", ": "))
        logger("Das Menu für den " + str(date[2]) + " wurde als JSON datei gespeichert", 2)


def subStart(argument):

    chatID = str(argument.message.chat_id)
    chatIDextra = getUserData(argument.message.chat)

    subPath = os.path.join(sys.path[0], config["MENU"]["subsciberfilename"])
    
    sub = configparser.RawConfigParser()
    sub.readfp(open(subPath))

    usrcount = sub.getint("INFO", "usrcount")
    subcount = sub.getint("INFO", "subcount")

    if not sub.has_section(chatID):

        logger("Für die ID" + chatID + " gibt es  keinen eintrag", 1)

        sub.add_section(chatID)
        sub.set(chatID, "userdata", chatIDextra)
        sub.set(chatID, "substartdate", getTime("dateEU"))
        sub.set(chatID, "substopdate", "-")
        sub.set(chatID, "resub", 0)
        sub.set(chatID, "act", 1)

        sub.set("INFO", "usrcount", usrcount + 1)
        sub.set("INFO", "subcount", subcount + 1)

        with open(subPath, "w") as subfile:
            sub.write(subfile)

        logger("Für die ID" + chatID + "wure ein neuer eintrag erstellt", 1)
        logger("Die Subscription von ID " + chatID + " wurde erstellt und ist nun aktiv", 2)

        return True

    else:

        if not sub.getint(chatID, "act"):

            sub.set("INFO", "subcount", subcount + 1),
            recount = sub.getint(chatID, "resub")

            sub.set(chatID, "userdata", chatIDextra)
            sub.set(chatID, "substartdate", getTime("dateEU"))
            sub.set(chatID, "substopdate", "-")
            sub.set(chatID, "resub", recount + 1)
            sub.set(chatID, "act", "1")

            with open(subPath, "w") as subfile:
                sub.write(subfile)

            logger("Die Subscription von ID " + chatID + " wurde erneuert und ist nun aktiv", 2)
            return True

        else:

            logger("Für die ID" + chatID + "gibt es bereits einen Aktiven eintrag", 1)
            return False


def subStop(argument):

    chatID = str(argument.message.chat_id)

    sub = configparser.RawConfigParser()
    sub.readfp(open(subPath))

    subcount = sub.getint("INFO", "subcount")

    if sub.has_section(chatID) and sub.getint(chatID, "act"):

        logger("Für die ID" + chatID + "wurde der eintrag gefunden", 1)

        sub.set(chatID, "substopdate", getTime("dateEU"))
        sub.set(chatID, "act", 0)

        sub.set("INFO", "subcount", subcount - 1)

        with open(subPath, "w") as subfile:
            sub.write(subfile)

        logger("Die Subscription von ID " + chatID + " wurde enfernt und ist nun gestoppt", 2)

        return True

    else:

        return False
