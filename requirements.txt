arrow==0.10.0
beautifulsoup4==4.6.0
lxml==4.0.0
requests==2.7.0
python-telegram-bot==10.1.0