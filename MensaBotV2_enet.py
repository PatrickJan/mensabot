import re
import requests

from bs4 import BeautifulSoup
from MensaBotV2_extra import logger, getTime


# Alle hier vorhandenen Funktionen sind für das Sammeln der Daten auf der Mensawebseite gedacht


def getMensaPlan(argument=0):  # Zieht das Menü von der Webseite und erzeugt ein fertig formatiertes Menü

    if(getTime("day", argument) == "Samstag" or getTime("day", argument) == "Sonntag"):

        logger("Am Wochenende ist die Mensa geschlossen", 2)
        return False

    else:
        url = "https://koeln.my-mensa.de/essen.php?v=4983499&hyp=1&lang=de&mensa=iwz#iwz_tag_" + getTime("doy", argument)

        response = requests.get(url, headers={'User-agent': 'TelegramMensa Bot'})

        if(response.status_code == 200):
            soup = BeautifulSoup(response.content, "lxml")
            rawDay = soup.find(attrs={"data-date2": getTime("date", argument)})

            try:
                rawMenuName = rawDay.find_all("li", {"style": "min-height:12px; padding-left:15px"})        # Name des Menüs(1,2,3)
                rawMenuInhalt = rawDay.find_all("h3", {"class": "ct ui-li-heading"})                        # Was es zum Essen gibt
                rawMenuExtra = rawDay.find_all(lambda tag: tag.name == 'p' and tag.get('class') == ['ct'])  # Extra inhalt des Menüs
                rawMenuPreis = rawDay.find_all("p", {"class": "ct next"})                                   # Preis für Studenten
            except AttributeError:
                logger("Konnte den Mensaplan für den " + getTime("dateEU", argument) + " nicht finden", 4)
                return False

            else:
                menuAll = prettifyRAW(rawMenuName, rawMenuInhalt, rawMenuExtra, rawMenuPreis)
                logger("Mensaplan für den " + getTime("dateEU", argument) + " wurde erstellt", 1)
                return menuAll

        else:
            logger("Konnte die Webseite der MENSA nicht aufrufen - StatusCode: " + str(response.status_code), 5)
            return False


def prettifyRAW(mn, mi, me, mp):  # das RAW Menü wird hier mithilfe von Regex und Replace Funktion in ein Array umgewandelt

    menuAll = []

    if(len(mn) == len(mi)):
        for i in range(len(mn)):
            menuAll.append([mn[i].get_text()] + [mi[i].get_text()] + [me[i].get_text()] + [mp[i].get_text()])

        for i in range(len(mn)):
            menuAll[i][0] = menuAll[i][0].strip()
            menuAll[i][1] = re.sub(r"\([^)]*\)", " ", menuAll[i][1]).replace('* ', '').replace('\xad', '').replace('Vegetarisch: ', '').replace(' und', ',').rstrip()
            menuAll[i][2] = re.sub(r"\([^)]*\)", " ", menuAll[i][2]).replace('* ', '').replace('\xad', '').replace('mit ', ',').rstrip()

            if(menuAll[i][2] != ''):
                menuAll[i][1] += " und dazu"

            menuAll[i][3] = menuAll[i][3].split('\xa0€')[0] + "€"

        return menuAll

    else:
        logger("Das Menu ist nicht gleich lang", 5)
        return False


def prettifyTelegram(argument=0):  # das Fertige Menü wird hier nochmal neu für Telegram formatiert

    rawMenu = getMensaPlan(argument)
    prettifyTel = ""

    if rawMenu:
        url = "https://koeln.my-mensa.de/essen.php?v=4983499&hyp=1&lang=de&mensa=iwz#iwz_tag_" + getTime("doy", argument)

        prettifyTel = "Das Menü am <a href='" + url + "'>" + getTime("day", argument) + "</a> den " + getTime("dateEU", argument) + ":\n\n"

        for i in range(len(rawMenu)):
                prettifyTel += "<b>" + rawMenu[i][0] + " - " + rawMenu[i][3] + "</b>\n"
                prettifyTel += rawMenu[i][1] + " " + rawMenu[i][2] + "\n\n"

        logger("Telegram Menu für den " + getTime("dateEU", argument) + " wurde erstellt", 1)

    else:
        prettifyTel = "Das Menü am " + getTime("dateEU", argument) + " konnte nicht gefunden werden.\nIst die Mensa an dem Tag vielleicht geschlossen?"

    return prettifyTel