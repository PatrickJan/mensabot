# README #
Mein TH Köln Python Mensa Bot!
Geschrieben in Python zum Abfragen des täglichen Mensa Plans
# Last Edit 28.08.2018

- MensaBotV2 Subsciption Fehler behoben

- Timezone eingestellt
- subscriber.cfg wird automatisch erstellt
- token.cfg mit logging verknüpft
- ColoredLogs Farben und Formatierung angepasst
- MensaBotV2_extra - Systemkompabilität geändert/verbessert und redundanten Code entfernt
- MensaBotV2_enet - Schreibfehler und Text verbessert

- MensaBotV2_main in MensaBotV2 umbenannt
- token_example.cfg und subscriber_example.cfg entfernt
- MensaBotV2 | extra | enet - Kommentaren hinzugefügt um eine Art von MVC hervorzuheben
- MensaBotV2_enet - prettifyTelegram Funktion mit Datum und Tag verbessert/geändert
- MensaBotV2_extra - token,subscriber,mensaplan und log Odner-Struktur entfernt und durch token.cfg ersetzt

